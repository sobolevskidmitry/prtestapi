const router = require('express').Router();

router.use('/products', require('./Product'));
router.use('/user', require('./Auth').router);

module.exports = router;
