const router = require('express').Router();
const ProductController = require('../controllers/Product');
const auth = require('./Auth').auth;

router.get('/', auth.required, ProductController.getAllProducts);
router.get('/:id', auth.required, ProductController.getProduct);
router.post('/', auth.required, ProductController.addProduct);
router.put('/:id', auth.required, ProductController.updateProduct);
router.post('/:id/remove', auth.required, ProductController.deleteProduct);


module.exports = router;
