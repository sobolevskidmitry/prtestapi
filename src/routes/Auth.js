const jwt = require('express-jwt');
const router = require('express').Router();
const User = require('../models/User');
const passport = require('passport');

const getTokenFromHeaders = (req) => {
  const {headers: {authorization}} = req;

  return authorization || null;
};

const auth = {
  required: jwt({
    secret: 'secret',
    userProperty: 'payload',
    getToken: getTokenFromHeaders,
  }),
  optional: jwt({
    secret: 'secret',
    userProperty: 'payload',
    getToken: getTokenFromHeaders,
    credentialsRequired: false,
  }),
};


router.post('/', auth.optional, (req, res, next) => {
  const {body: {user}} = req;

  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: 'is required',
      },
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: 'is required',
      },
    });
  }

  const finalUser = new User(user);

  finalUser.setPassword(user.password);

  return finalUser.save()
    .then(() => res.json({user: finalUser.toAuthJSON()}));
});

router.post('/login', auth.optional, (req, res, next) => {
  const {body: {user}} = req;

  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: 'is required',
      },
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: 'is required',
      },
    });
  }

  return passport.authenticate('local', {session: false}, (err, passportUser, info) => {
    if (err) {
      return next(err);
    }

    if (passportUser) {
      const user = passportUser;
      user.token = passportUser.generateJWT();

      return res.json({user: user.toAuthJSON()});
    }

    return res.status(401).json({
      errors: {
        auth: 'bad credentials',
      },
    });
  })(req, res, next);
});

router.get('/current', auth.required, (req, res, next) => {
  const {payload: {id}} = req;

  return User.findById(id)
    .then((user) => {
      if (!user) {
        return res.sendStatus(400);
      }

      return res.json({user: user.toAuthJSON()});
    });
});

module.exports = {router, auth};
