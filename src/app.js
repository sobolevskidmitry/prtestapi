const express = require("express");
const bodyParser = require("body-parser");
const config = require('./config');
const cors = require('cors');
require('./passport');

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.set("port", process.env.PORT || config.port);
app.use(bodyParser.json());
app.use(cors());
app.use(require('./routes'));

app.use((err, req, res, next) => {
  if(err.name === 'UnauthorizedError') {
    res.status(err.status).send({message:err.message});
    return;
  }

  next();
});


module.exports = app;
