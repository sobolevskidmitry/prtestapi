const mongoose = require('../database/db');

const ProductModel = mongoose.model('Product', {
  image: String,
  quantity: Number,
  name: String,
  price: Number,
  userId: String
});

module.exports = ProductModel;
