const app = require('./app');

const server = app.listen(app.get('port'), () => {
  console.log('Listening on port ' + server.address().port);
});
