const mongoose = require('mongoose');
const config = require('../config');

mongoose.connect(config.bdUrl, {useNewUrlParser: true});

module.exports = mongoose;
