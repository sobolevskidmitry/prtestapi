const ProductModel = require('../models/Product');

async function addProduct(req, res) {
  const {payload: {id}, body} = req;

  if (!body) {
    res.json({error: 'Empty request body'});
    return;
  }

  res.json(await ProductModel.create({...body, userId: id}));
}

async function deleteProduct(req, res) {
  const {params: {id}} = req;

  res.json(await ProductModel.findByIdAndRemove(id));
}

async function getProduct(req, res) {
  const {params: {id}} = req;

  res.json(await ProductModel.findById(id));
}

async function getAllProducts(req, res) {
  const {payload: {id}} = req;

  res.json(await ProductModel.find({userId: id}));
}

async function updateProduct(req, res) {
  const {params: {id}, body} = req;

  if (!body) {
    res.json({error: 'Empty request body'});
    return;
  }

  res.json(await ProductModel.findByIdAndUpdate(id, body, {new: true}));
}


module.exports = {
  addProduct: addProduct,
  getAllProducts: getAllProducts,
  getProduct: getProduct,
  deleteProduct: deleteProduct,
  updateProduct: updateProduct
};
